FROM python:3.8-slim-buster
COPY ./app.py /app/app.py
COPY ./model_importer.py /app/model_importer.py
COPY  ./requirements.txt /app/requirements.txt
COPY ./models/ /app/models/
WORKDIR /app
RUN apt-get update && apt-get install -y libgl1
RUN apt-get install -y libglib2.0-0
RUN pip install -r requirements.txt
ENTRYPOINT [ "streamlit", "run" ]
CMD [ "app.py" ]
