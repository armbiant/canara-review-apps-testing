import pandas as pd
from PIL import Image
import streamlit as st
from streamlit_drawable_canvas import st_canvas
import numpy as np
import PIL.ImageOps

# Specify canvas parameters in application
#stroke_width = st.sidebar.slider("Stroke width: ", 1, 25, 3)
#stroke_color = st.sidebar.color_picker("Stroke color hex: ")
#bg_color = st.sidebar.color_picker("Background color hex: ", "#eee")
#bg_image = st.sidebar.file_uploader("Background image:", type=["png", "jpg"])
#drawing_mode = st.sidebar.selectbox("Drawing tool:", ("freedraw", "line", "rect", "circle", "transform"))
#realtime_update = st.sidebar.checkbox("Update in realtime", True)

# Create a canvas component
canvas_result = st_canvas(
    fill_color="rgba(255, 165, 0, 0.3)",  # Fixed fill color with some opacity
    stroke_width=7,
    stroke_color="rgba(0, 0, 0, 1)",
    background_color="#d3d3d3",
    background_image=None,
    update_streamlit=True,
    height=150,
    drawing_mode="freedraw",
    key="canvas",
)
im = canvas_result.image_data
print(im)
im = Image.fromarray((im * 255).astype(np.uint8))
im = im.convert('RGB')
#im = PIL.ImageOps.invert(im)
im.save("test1.png")

# Do something interesting with the image data and paths
#if canvas_result.image_data is not None:
    #st.image(canvas_result.image_data)
#if canvas_result.json_data is not None:
    #st.dataframe(pd.json_normalize(canvas_result.json_data["objects"]))