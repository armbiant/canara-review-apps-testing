"""
Downloads model from Comet Registry using Python SDK

Author William Arias

app v 1.0


"""
import os
import argparse
from comet_ml import API


def get_args():
    """Gets arguments passed in GitLab CI"""

    parser = argparse.ArgumentParser()
    parser.add_argument("--workspace", type=str)
    parser.add_argument("--registry_name", type=str)
    parser.add_argument("--version", type=str)
    parser.add_argument("--expand", type=bool)

    return parser.parse_args()


def model_importer(workspace, registry_name, version):
    try:
        api = API(api_key=os.environ["COMET_API_KEY"])
    except:
        print("Using API Key local")
        api = API()

    output_path ="./models"
    api.download_registry_model(workspace=workspace, registry_name=registry_name,
                                version=version, expand=True, output_path=output_path)


def main():
    args = get_args()
    workspace = args.workspace
    registry_name = args.registry_name
    version = args.version

    model_importer(workspace, registry_name, version)


if __name__ == "__main__":
    main()

