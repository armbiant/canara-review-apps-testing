"""
Model Loader and predictions using Streamlit and bringing a model from Comet Model registry

Author William Arias

app v 1.0


"""

import os
import cv2
import numpy as np
import streamlit as st
from streamlit_drawable_canvas import st_canvas
from tensorflow.keras.models import load_model

COMET_MODEL_PATH = "models/"


@st.cache(allow_output_mutation=True)
def model_loader(model_path):
    """ Loads model downloaded from Comet Model registry """
    model_name = "mnist-nn.h5"
    model = os.path.join(model_path, model_name)
    model = load_model(model)

    return model


def canvas_to_draw():
    """ Renders the canvas where end users can draw numbers  """
    output_canvas_size = 250
    canvas_result = st_canvas(
        fill_color='#000000',
        stroke_width=15,
        stroke_color='#FFFFFF',
        background_color='#000000',
        width=output_canvas_size,
        height=output_canvas_size,
        drawing_mode="freedraw",
        display_toolbar=True,
        key='canvas')

    return canvas_result.image_data


def digit_predictor(image_data, model):
    """Gets the number entered by user, resizes and returns prediction"""
    if image_data is not None:
        unseen_image = cv2.resize(image_data.astype('float32'), (28, 28))

    if st.button('Predict Digit'):

        unseen_image = cv2.cvtColor(unseen_image, cv2.COLOR_BGR2GRAY)
        digit_value = model.predict(unseen_image.reshape(1, 784))

        st.markdown(f"#### I think you drew a: {np.argmax(digit_value[0])}")


def main():
    """Executes all  functions"""

    st.header("GitLab DevOps Platform & Comet-ml")
    st.subheader("Draw a number and click on 'Predict Digit'")

    loaded_model = model_loader(COMET_MODEL_PATH)
    image = canvas_to_draw()
    digit_predictor(image, loaded_model)


if __name__ == "__main__":
    main()
