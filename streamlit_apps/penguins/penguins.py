import matplotlib.pyplot as plt
import streamlit as st
import pandas as pd
import seaborn as sns

st.title("Penguins Dataset")

# Data import from 'data' folder
DATASET = 'data/penguins.csv'

# Markdown
st.markdown("Create Scatterplots about penguins")

# species selection
selected_species = st.selectbox("What species do you want to visualize?", ['Adelie', 'Gentoo', 'Chinstrap'])

# Variables selection
select_x_vr = st.selectbox("Select x variable for the plot",
                           ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g'])
select_y_vr = st.selectbox("Select y variable for the plot",
                           ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g'])


#Use sns darkgrid
sns.set_style('darkgrid')

# Plot each category differently
markers = {"Adelie": "X", "Gentoo": "s", "Chinstrap":'o'}
# With variables selected, filter dataframe
custom_file = st.file_uploader("Select your file")
penguins_df = pd.read_csv(DATASET)

# Use species to filter out dataframe
# Deactivate this line to load all the data in the plot
#penguins_df = penguins_df[penguins_df['species'] == selected_species]

# fig and axes matplotlib
fig, ax = plt.subplots()
ax = sns.scatterplot(data=penguins_df, x=penguins_df[select_x_vr],
                     y=penguins_df[select_y_vr], hue='species', markers= markers, style='species')

plt.xlabel("Selected X variable")
plt.ylabel("Selected Y variable")
plt.title(f"Scatterplot of {selected_species}")
st.pyplot(fig)



# write first rows
#st.write(penguins_df['species'] =='Gentoo')