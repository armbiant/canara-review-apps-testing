"""Script used to create the ML Model"""

import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

DATASET = "data/penguins.csv"

penguin_df = pd.read_csv(DATASET)
penguin_df.dropna(inplace=True)

output = penguin_df['species']
features = penguin_df[['island', 'bill_length_mm', 'bill_depth_mm',
                       'flipper_length_mm', 'body_mass_g', 'sex']]

features = pd.get_dummies(features)
output, uniques = pd.factorize(output)

x_train, x_test, y_train, y_test = train_test_split(features, output, test_size=.8)
rfc = RandomForestClassifier(random_state=15)
rfc.fit(x_train, y_train)
y_predicted = rfc.predict(x_test)
score = accuracy_score(y_predicted, y_test)
print(f"accuracy for this model is {score}")
print("output variables")
print(output, "\n")
print("feature variables")
print(features.head())
